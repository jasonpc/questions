//
//  AppDelegate.swift
//  Questions
//
//  Created by Jason Chalecki on 11/17/15.
//  Copyright © 2015 Jason Chalecki. All rights reserved.
//

import UIKit

typealias JSONObject = Dictionary<String, AnyObject>


class Question {
    
    let identifier: String
    let text: String
    let imageURL: NSURL?
    let answers: [String]
    let correctAnswerIndex: Int
    
    init() {
        // Only for use when an initializer fails
        self.identifier = ""
        self.text = ""
        self.imageURL = nil
        self.answers = []
        self.correctAnswerIndex = 0
    }
    
    init?(json: JSONObject) {
        guard let identifier = json["id"] as? String,
            let text = json["body"] as? String,
            let rawAnswers = json["choices"] as? [JSONObject] else {
            self.identifier = ""
            self.text = ""
            self.imageURL = nil
            self.answers = []
            self.correctAnswerIndex = 0
            return nil
        }
        
        var answers: [String] = []
        var correctAnswerIndex = 0
        for info in rawAnswers {
            guard let text = info["body"] as? String,
                let isCorrect = info["correct"] as? Bool else {
                self.identifier = ""
                self.text = ""
                self.imageURL = nil
                self.answers = []
                self.correctAnswerIndex = 0
                return nil
            }
            answers.append(text)
            if isCorrect {
                correctAnswerIndex = answers.count - 1
            }
        }
        
        self.answers = answers
        self.correctAnswerIndex = correctAnswerIndex

        self.identifier = identifier
        self.text = text
        self.imageURL = NSURL(string: json["image"] as? String ?? "")
        
        guard self.answers.count > 0 else {
            return nil
        }
    }
    
    func answerNameForIndex(index: Int) -> String {
        return String(UnicodeScalar(index + Int("A".unicodeScalars.first!.value)))
    }
}

class Response {
    
    unowned let askedQuestion: AskedQuestion
    let cardId: Int
    let chosenAnswerIndex: Int
    
    init?(askedQuestion: AskedQuestion, json: JSONObject) {
        guard let cardId = json["card"] as? Int,
            let student = json["student"] as? String,
            let chosenAnswer = json["answer"] as? String else {
            self.askedQuestion = askedQuestion
            self.cardId = 0
            self.chosenAnswerIndex = 0
            return nil
        }
        
        self.askedQuestion = askedQuestion
        self.cardId = cardId
        self.chosenAnswerIndex = Int(chosenAnswer.unicodeScalars.first!.value - "A".unicodeScalars.first!.value)
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).studentsByCardId[cardId] = student
    }
}

class AskedQuestion {
    
    static let DidFinishLoadingNotification = "AskedQuestion.DidFinishLoadingNotification"
    
    let identifier: String
    let createdDate: NSDate
    let question: Question
    var responsesByCardId: [Int: Response] = [:]
    
    init?(json: JSONObject) {
        guard let identifier = json["id"] as? String,
            let createdString = json["created"] as? String,
            let rawQuestion = json["question"] as? JSONObject,
            let question = Question(json: rawQuestion),
            let rawResponses = json["responses"] as? [JSONObject] else {
            self.identifier = ""
            self.createdDate = NSDate.distantPast()
            self.question = Question()
            return nil
        }
        
        self.identifier = identifier
        self.createdDate = (UIApplication.sharedApplication().delegate as! AppDelegate).dateFormatter.dateFromString(createdString) ?? NSDate.distantPast()
        self.question = question
        
        for raw in rawResponses {
            if let response = Response(askedQuestion: self, json: raw) {
                self.responsesByCardId[response.cardId] = response
            }
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var dateFormatter: NSDateFormatter!
    var session: NSURLSession!
    
    var askedQuestionsMap: [String: AskedQuestion] = [:]
    var studentsByCardId: [Int: String] = [:]
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window = UIWindow()
        self.window!.rootViewController = UINavigationController(rootViewController: ViewController())
        self.window!.makeKeyAndVisible()
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        self.dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        self.dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
        
        self.session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        loadQuestions()
        
        return true
    }
    
    func loadQuestions() {
        self.session.dataTaskWithURL(NSURL(string: "http://plickers-interview.herokuapp.com/polls")!) { (possibleData, response, error) -> Void in
            do {
                guard let data = possibleData,
                    let list = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0)) as? [JSONObject] else {
                    return
                }
                
                var map: [String: AskedQuestion] = [:]
                for json in list {
                    if let askedQuestion = AskedQuestion(json: json) {
                        map[askedQuestion.identifier] = askedQuestion
                    }
                }
                
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    self.askedQuestionsMap = map
                    NSNotificationCenter.defaultCenter().postNotificationName(AskedQuestion.DidFinishLoadingNotification, object: nil)
                }
            }
            catch {
            }
        }.resume()
    }
}

