//
//  ViewController.swift
//  Questions
//
//  Created by Jason Chalecki on 11/17/15.
//  Copyright © 2015 Jason Chalecki. All rights reserved.
//

import UIKit


class SubtitleTableViewCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class FullImageTableViewCell: UITableViewCell {
    
    let fullImageView: UIImageView
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        self.fullImageView = UIImageView()
        
        super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
        
        self.fullImageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.fullImageView)
        
        self.contentView.addConstraint(NSLayoutConstraint(item: self.fullImageView, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: self.contentView, attribute: .Left, multiplier: 1.0, constant: 0.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.fullImageView, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: self.contentView, attribute: .Right, multiplier: 1.0, constant: 0.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.fullImageView, attribute: .Top, relatedBy: .Equal, toItem: self.contentView, attribute: .Top, multiplier: 1.0, constant: 0.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.fullImageView, attribute: .Bottom, relatedBy: .Equal, toItem: self.contentView, attribute: .Bottom, multiplier: 1.0, constant: 0.0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class FullQuestionController: UITableViewController {

    private let question: Question
    private var cachedImage: UIImage?
    
    init(question: Question) {
        self.question = question
        super.init(style: .Plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        self.tableView.registerClass(FullImageTableViewCell.self, forCellReuseIdentifier: "FullImageTableViewCell")
        
        if let imageURL = self.question.imageURL {
            (UIApplication.sharedApplication().delegate as! AppDelegate).session.dataTaskWithURL(imageURL, completionHandler: { (data, response, error) -> Void in
                if data?.length > 0,
                    let image = UIImage(data: data!) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.cachedImage = image
                        self.tableView.reloadData()
                    }
                }
            }).resume()
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.question.text.isEmpty ? 0 : 1) + (self.question.imageURL == nil ? 0 : 1) + self.question.answers.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if !self.question.text.isEmpty && indexPath.row == 0 {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.text = self.question.text
            return cell
        } else if self.question.imageURL != nil && ((self.question.text.isEmpty && indexPath.row == 0) || (!self.question.text.isEmpty && indexPath.row == 1)) {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("FullImageTableViewCell", forIndexPath: indexPath) as! FullImageTableViewCell
            if let image = self.cachedImage {
                cell.fullImageView.image = image
                cell.textLabel!.text = nil
            } else {
                cell.textLabel!.text = "(Loading image...)"
            }
            return cell
        } else {
            let answerIndex = indexPath.row - (self.question.text.isEmpty ? 0 : 1) - (self.question.imageURL == nil ? 0 : 1)
            let cell = self.tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.text = "\(self.question.answerNameForIndex(answerIndex)): " + self.question.answers[answerIndex]
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


class QuestionController: UITableViewController {
    
    private let askedQuestion: AskedQuestion
    private let rightResponses: [Response]
    private let wrongResponses: [Response]
    private let studentsByCardId: [Int: String]
    
    init(askedQuestion: AskedQuestion, studentsByCardId: [Int: String]) {
        self.askedQuestion = askedQuestion
        
        var rightResponses: [Response] = []
        var wrongResponses: [Response] = []
        
        for response in askedQuestion.responsesByCardId.values {
            if response.chosenAnswerIndex == askedQuestion.question.correctAnswerIndex {
                rightResponses.append(response)
            } else {
                wrongResponses.append(response)
            }
        }
        
        self.rightResponses = rightResponses.sort() { studentsByCardId[$0.cardId]! < studentsByCardId[$1.cardId]! }
        self.wrongResponses = wrongResponses.sort() { studentsByCardId[$0.cardId]! < studentsByCardId[$1.cardId]! }
        
        self.studentsByCardId = studentsByCardId
        
        super.init(style: .Plain)
        self.navigationItem.title = self.askedQuestion.question.text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        self.tableView.registerClass(SubtitleTableViewCell.self, forCellReuseIdentifier: "SubtitleTableViewCell")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1 + self.askedQuestion.question.answers.count
        case 1:
            return max(self.wrongResponses.count, 1)
        case 2:
            return max(self.rightResponses.count, 1)
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier((indexPath.section == 0 ? "UITableViewCell" : "SubtitleTableViewCell"), forIndexPath: indexPath)
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                cell.textLabel!.numberOfLines = 0
                cell.textLabel?.text = self.askedQuestion.question.text
            } else {
                let question = self.askedQuestion.question
                cell.textLabel!.numberOfLines = 0
                cell.textLabel?.text = "\(question.answerNameForIndex(indexPath.row - 1)): " + question.answers[indexPath.row - 1]
            }
        case 1:
            let response = self.wrongResponses[indexPath.row]
            cell.textLabel!.numberOfLines = 1
            cell.textLabel?.text = self.studentsByCardId[response.cardId]
            cell.detailTextLabel!.text = self.askedQuestion.question.answerNameForIndex(response.chosenAnswerIndex)
            break
        case 2:
            let response = self.rightResponses[indexPath.row]
            cell.textLabel!.numberOfLines = 1
            cell.textLabel?.text = self.studentsByCardId[response.cardId]
            cell.detailTextLabel!.text = self.askedQuestion.question.answerNameForIndex(response.chosenAnswerIndex)
            break
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let total = Double(self.rightResponses.count + self.wrongResponses.count)
        switch section {
        case 0:
            return nil
        case 1:
            return "Wrong answers: \(self.wrongResponses.count) (\(Int(total == 0 ? 0 : round(100.0 * Double(self.wrongResponses.count) / total)))%)"
        case 2:
            return "Right answers: \(self.rightResponses.count) (\(Int(total == 0 ? 0 : round(100.0 * Double(self.rightResponses.count) / total)))%)"
        default:
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return (indexPath.section == 0 && indexPath.row == 0)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.pushViewController(FullQuestionController(question: self.askedQuestion.question), animated: true)
    }
}


class AllQuestionsController: UITableViewController {
    
    private let sortedAskedQuestionIds: [String]
    private let askedQuestionsMap: [String: AskedQuestion]
    private let studentsByCardId: [Int: String]
    
    init() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let askedQuestionsMap = appDelegate.askedQuestionsMap
        self.askedQuestionsMap = askedQuestionsMap
        self.sortedAskedQuestionIds = askedQuestionsMap.keys.sort() { askedQuestionsMap[$0]!.createdDate < askedQuestionsMap[$1]!.createdDate }
        self.studentsByCardId = appDelegate.studentsByCardId
        
        super.init(style: .Plain)
        self.navigationItem.title = "Questions"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortedAskedQuestionIds.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
        cell.textLabel!.text = self.askedQuestionsMap[self.sortedAskedQuestionIds[indexPath.row]]!.question.text 
        cell.accessoryType = .DisclosureIndicator
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let askedQuestion = self.askedQuestionsMap[self.sortedAskedQuestionIds[indexPath.row]]!
        self.navigationController?.pushViewController(QuestionController(askedQuestion: askedQuestion, studentsByCardId: self.studentsByCardId), animated: true)
    }
}


class StudentController: UITableViewController {
    
    private let cardId: Int
    private let student: String
    private let rightResponses: [Response]
    private let wrongResponses: [Response]
    
    init(cardId: Int, student: String, responses: [Response]) {
        self.cardId = cardId
        self.student = student
        
        var rightResponses: [Response] = []
        var wrongResponses: [Response] = []
        
        for response in responses {
            if response.chosenAnswerIndex == response.askedQuestion.question.correctAnswerIndex {
                rightResponses.append(response)
            } else {
                wrongResponses.append(response)
            }
        }
        
        self.rightResponses = rightResponses.sort() { $0.askedQuestion.createdDate < $1.askedQuestion.createdDate }
        self.wrongResponses = wrongResponses.sort() { $0.askedQuestion.createdDate < $1.askedQuestion.createdDate }
        
        super.init(style: .Plain)
        self.navigationItem.title = self.student
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.registerClass(SubtitleTableViewCell.self, forCellReuseIdentifier: "SubtitleTableViewCell")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return max(self.wrongResponses.count, 1)
        case 1:
            return max(self.rightResponses.count, 1)
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SubtitleTableViewCell", forIndexPath: indexPath)
        switch indexPath.section {
        case 0:
            let response = self.wrongResponses[indexPath.row]
            let question = response.askedQuestion.question
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.text = question.text
            cell.detailTextLabel!.numberOfLines = 0
            cell.detailTextLabel!.text = "\(question.answerNameForIndex(response.chosenAnswerIndex)): " + question.answers[response.chosenAnswerIndex]
        case 1:
            let response = self.rightResponses[indexPath.row]
            let question = response.askedQuestion.question
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.text = question.text
            cell.detailTextLabel!.numberOfLines = 0
            cell.detailTextLabel!.text = "\(question.answerNameForIndex(response.chosenAnswerIndex)): " + question.answers[response.chosenAnswerIndex]
        default:
            break
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let total = Double(self.rightResponses.count + self.wrongResponses.count)
        switch section {
        case 0:
            return "Wrong answers: \(self.wrongResponses.count) (\(Int(total == 0 ? 0 : round(100.0 * Double(self.wrongResponses.count) / total)))%)"
        case 1:
            return "Right answers: \(self.rightResponses.count) (\(Int(total == 0 ? 0 : round(100.0 * Double(self.rightResponses.count) / total)))%)"
        default:
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var response: Response!
        switch indexPath.section {
        case 0:
            response = self.wrongResponses[indexPath.row]
        case 1:
            response = self.rightResponses[indexPath.row]
        default:
            break
        }
        
        let question = response.askedQuestion.question
        self.navigationController?.pushViewController(FullQuestionController(question: question), animated: true)
    }
}


class AllStudentsController: UITableViewController {
    
    private let sortedCardIds: [Int]
    private let studentsByCardId: [Int: String]
    private let askedQuestionsMap: [String: AskedQuestion]
    
    init() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.studentsByCardId = appDelegate.studentsByCardId
        self.sortedCardIds = studentsByCardId.keys.sort() { appDelegate.studentsByCardId[$0]! < appDelegate.studentsByCardId[$1]! }
        self.askedQuestionsMap = appDelegate.askedQuestionsMap
        
        super.init(style: .Plain)
        self.navigationItem.title = "Students"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortedCardIds.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
        cell.textLabel!.text = self.studentsByCardId[self.sortedCardIds[indexPath.row]]!
        cell.accessoryType = .DisclosureIndicator
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cardId = self.sortedCardIds[indexPath.row]
        let student = self.studentsByCardId[cardId]!
        
        let responses = Array(self.askedQuestionsMap.values.filter() { $0.responsesByCardId[cardId] != nil }.map() { $0.responsesByCardId[cardId]! })
        
        self.navigationController?.pushViewController(StudentController(cardId: cardId, student: student, responses: responses), animated: true)
    }
}


class ViewController: UITableViewController {
    
    private var dataLoaded = false
    
    init() {
        super.init(style: .Plain)
        self.navigationItem.title = "Asked Questions"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handleAskedQuestionDidFinishLoading(notification: NSNotification) {
        self.dataLoaded = true
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        if (UIApplication.sharedApplication().delegate as! AppDelegate).askedQuestionsMap.count > 0 {
            self.dataLoaded = true
        } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleAskedQuestionDidFinishLoading:", name: AskedQuestion.DidFinishLoadingNotification, object: nil)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataLoaded ? 2 : 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
        switch indexPath.row {
        case 0:
            if self.dataLoaded {
                cell.textLabel!.text = "Questions"
                cell.textLabel!.textAlignment = .Left
                cell.accessoryType = .DisclosureIndicator
            } else {
                cell.textLabel!.text = "Loading questions..."
                cell.textLabel!.textAlignment = .Center
                cell.accessoryType = .None
            }
        case 1:
            cell.textLabel!.text = "Students"
            cell.textLabel!.textAlignment = .Left
            cell.accessoryType = .DisclosureIndicator
        default:
            cell.textLabel!.text = ""
            cell.textLabel!.textAlignment = .Left
            cell.accessoryType = .None
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return self.dataLoaded
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController(AllQuestionsController(), animated: true)
        case 1:
            self.navigationController?.pushViewController(AllStudentsController(), animated: true)
        default:
            break
        }
    }
}

extension NSDate : Comparable {
}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedSame
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending
}

public func >(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedDescending
}
